import {Injectable} from "@nestjs/common";
import {GqlModuleOptions, GqlOptionsFactory} from "@nestjs/graphql";
import {join} from "path";
import {ConfigService} from "@nestjs/config";

@Injectable()
export class GqlConfigService implements GqlOptionsFactory {
  constructor(private configService: ConfigService) {
  }

  createGqlOptions(): GqlModuleOptions {
    const debug = this.configService.get("DEBUG")
    const playground = this.configService.get("PLAYGROUND")

    return {
      debug,
      playground,
      typePaths: [join(process.cwd(), '../schemas/**/*.graphql')],
      definitions: {
        path: join(process.cwd(), 'src/types.generated.ts'),
        emitTypenameField: false,
        outputAs: 'interface'
      }
    };
  }
}
