import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {GraphQLModule} from "@nestjs/graphql";
import {GqlConfigService} from "./gqlConfigService";
import {RecipesService} from './recipes/recipes.service';
import {RecipesResolver} from './recipes/recipes.resolver';
import {ConfigModule} from './config/config.module';
import {DatabaseModule} from './database/database.module';
import {RecipesModule} from './recipes/recipes.module';
import {UsersModule} from './users/users.module';

@Module({
  imports: [
    ConfigModule,
    DatabaseModule,
    GraphQLModule.forRootAsync({
      useClass: GqlConfigService,
    }),
    RecipesModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService, RecipesService, RecipesResolver],
})
export class AppModule {
  constructor() {
  }
}
