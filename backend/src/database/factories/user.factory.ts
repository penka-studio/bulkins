import {User} from '../../users/user.entity';
import {define} from 'typeorm-seeding';
import * as Faker from 'faker';

define(User, (faker: typeof Faker) => {
  const user = new User();

  user.firstName = faker.random.word();
  user.lastName = faker.random.word();
  user.isActive = faker.random.boolean();

  return user;
});
