import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: ['../schemas/**/*.graphql'],
  path: join(process.cwd(), 'src/types.generated.ts'),
  emitTypenameField: true,
  watch: true
});
