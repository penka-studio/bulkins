import { Resolver, Query } from '@nestjs/graphql';
import {RecipesService} from './recipes.service';
import {Recipe, RecipesQuery} from '../types.generated';

@Resolver('Recipe')
export class RecipesResolver {
  constructor(
    private recipesService: RecipesService,
  ) {}

  @Query()
  async recipes() {
    return [{
      id: "0",
      name: "Name",
      notes: "sad"
    }]
  }
}
