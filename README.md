# Bulkins
## Backend

1. Устанавливаем poetry:

    ```
    pip install --user poetry
    ```

    ```
    $PATH=$PATH:$HOME/.poetry/bin
    ```

2. Разрешаем создание виртуального окружения в проекте:
    
    ```
    poetry config settings.virtualenvs.in-project true
    ```

3. В проекте уже инициализируем окружение:
    
   ```
   python3 -m venv .venv
   ```
   
4. Устанавливаем зависимости и разворачиваем окружение:

   ```
   poetry install
   ```

5. Запускаем сервер:

   ```
   gunicorn bulkins.wsgi
   ```