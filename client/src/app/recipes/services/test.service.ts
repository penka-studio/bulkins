import { Injectable } from '@angular/core';
import {GetRecipesGQL} from "../../../types.generated";

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private getRecipesGQL: GetRecipesGQL) { }

  getRecipes() {
    return this.getRecipesGQL.watch().valueChanges
  }
}
