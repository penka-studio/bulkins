export const environment = {
  production: true,
  server: process.env.SERVER_URL,
  environment: process.env.ENVIRONMENT,
  version: process.env.VERSION
};
