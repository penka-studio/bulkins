FROM node:15.0.1-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install

COPY ../backend .

COPY ../schemas ../

RUN yarn build
