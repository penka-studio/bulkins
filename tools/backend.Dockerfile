FROM node:14

WORKDIR /usr/src/app

COPY backend/package.json ./
COPY client/yarn.lock ./

RUN yarn install

COPY backend/ ./

COPY schemas/ ../schemas/
