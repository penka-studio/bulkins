FROM node:14

WORKDIR /usr/src/app

COPY client/package.json ./
COPY client/yarn.lock ./

RUN yarn install

COPY client/ ./
